require 'test_helper'

class AssignEmployeesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @assign_employee = assign_employees(:one)
  end

  test "should get index" do
    get assign_employees_url
    assert_response :success
  end

  test "should get new" do
    get new_assign_employee_url
    assert_response :success
  end

  test "should create assign_employee" do
    assert_difference('AssignEmployee.count') do
      post assign_employees_url, params: { assign_employee: { assign_date: @assign_employee.assign_date, client: @assign_employee.client, comments: @assign_employee.comments, employee: @assign_employee.employee } }
    end

    assert_redirected_to assign_employee_url(AssignEmployee.last)
  end

  test "should show assign_employee" do
    get assign_employee_url(@assign_employee)
    assert_response :success
  end

  test "should get edit" do
    get edit_assign_employee_url(@assign_employee)
    assert_response :success
  end

  test "should update assign_employee" do
    patch assign_employee_url(@assign_employee), params: { assign_employee: { assign_date: @assign_employee.assign_date, client: @assign_employee.client, comments: @assign_employee.comments, employee: @assign_employee.employee } }
    assert_redirected_to assign_employee_url(@assign_employee)
  end

  test "should destroy assign_employee" do
    assert_difference('AssignEmployee.count', -1) do
      delete assign_employee_url(@assign_employee)
    end

    assert_redirected_to assign_employees_url
  end
end
