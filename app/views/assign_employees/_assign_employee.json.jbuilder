json.extract! assign_employee, :id, :client, :employee, :assign_date, :comments, :created_at, :updated_at
json.url assign_employee_url(assign_employee, format: :json)