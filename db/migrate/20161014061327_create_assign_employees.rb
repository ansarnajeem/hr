class CreateAssignEmployees < ActiveRecord::Migration[5.0]
  def change
    create_table :assign_employees do |t|
      t.string :client
      t.string :employee
      t.date :assign_date
      t.text :comments

      t.timestamps
    end
  end
end
